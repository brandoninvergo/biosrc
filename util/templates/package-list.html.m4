TITLE({"BioSRC Package List"})
BODY({"{"

This is a list of all the packages that are currently present in
BioSRC. This list is automatically updated weekly to reflect the
latest software versions or package additions. The "BioSRC Name" of a
package is the one that you would use to install the package via
BioSRC, i.e. <code>make -C pkg/bio/hmmer install</code>. The "Name" of
the package is its canonical name.

**Last update**: UPDATE_DATE

<table id="packagelist">
<tr id="packagerow">
<th id="packagehead", width="15%">Name</th>
<th id="packagehead", width="25%">BioSRC Name</th>
<th id="packagehead", width="50%">Description</th>
<th id="packagehead", width="10%">Version</th>
</tr>
PACKAGE_LIST
</table>

"}"})
