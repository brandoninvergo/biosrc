#!/bin/bash

if [ $# -ne 3 ]; then
    echo "Usage: ./new_project.sh [subdir] [name] [version]"
    echo "Example: ./new_project.sh bio hmmer 1.3b1"
    exit 1
fi
mkdir -p ../pkg/${1}/${2}
m4 -D__NAME=${2} -D__VERSION=${3} templates/Makefile.m4 > ../pkg/${1}/${2}/Makefile
m4 -D__NAME=${2} templates/config.mk.m4 > ../pkg/${1}/${2}/config.mk
exit 0
