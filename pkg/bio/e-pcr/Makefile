# Copyright © 2013, 2014 Brandon Invergo <brandon@invergo.net>
#
# This file is part of GSRC.
#
# GSRC is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GSRC is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GSRC.  If not, see <http://www.gnu.org/licenses/>.

NAME = e-PCR
GARNAME = e-pcr
UPSTREAMNAME = e-PCR
GARVERSION = 2.3.12-1
HOME_URL = https://www.ncbi.nlm.nih.gov/tools/epcr/
DESCRIPTION = Electronic PCR
define BLURB
e-PCR identifies sequence tagged sites (STS) within DNA sequences,
allowing you to search for sub-sequences that closely match PCR
primers.
endef
LICENSE = Public Domain
CITE = PMID:9149949

######################################################################

MASTER_SITES = ftp://ftp.ncbi.nlm.nih.gov/
MASTER_SUBDIR = pub/schuler/$(UPSTREAMNAME)/
DISTFILES = $(DISTNAME)-src.tar.gz
WORKSRC = $(WORKDIR)/$(subst -1,,$(DISTNAME))
BUILD_SCRIPTS = $(WORKSRC)/Makefile
INSTALL_SCRIPTS = $(WORKSRC)/Makefile

BUILDDEPS =
LIBDEPS =

######################################################################

include ../../../gar/gar.mk
include config.mk

BUILD_ARGS += links depend all OPTIMIZE=6 prefix=$(prefix)
INSTALL_ARGS += dirs install

pre-build:
	sed -i 's/^LDFLAGS =/MYLDFLAGS = $$(LDFLAGS)/' $(WORKSRC)/stand/config.mk
	for mf in $(WORKSRC)/stand/Makefile*; do \
		sed -i 's/$$(LDFLAGS)/$$(MYLDFLAGS)/g' $$mf; \
	done
	$(MAKECOOKIE)

