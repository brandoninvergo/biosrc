# Copyright © 2013, 2014, 2015, 2016 Brandon Invergo <brandon@invergo.net>
#
# This file is part of GSRC.
#
# GSRC is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GSRC is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GSRC.  If not, see <http://www.gnu.org/licenses/>.

NAME = The GNU Compiler Collection
GARNAME = gcc
GARVERSION = 6.1.0
HOME_URL = http://www.gnu.org/software/gcc
DESCRIPTION = GNU Compiler Collection
define BLURB
GCC is the GNU Compiler Collection.  It provides compiler front-ends
for several languages, including C, C++, Objective-C, Fortran, Java,
Ada, and Go.  It also includes runtime support libraries for these
languages.
endef

######################################################################

MASTER_SITES = $(MASTER_GNU)
MASTER_SUBDIR = $(GARNAME)/$(DISTNAME)/
DISTFILES = $(DISTNAME).tar.gz
SIGFILES = $(DISTNAME).tar.gz.sig
WORKOBJ = $(WORKDIR)/build-$(GARVERSION)
INSTALL_SCRIPTS = $(WORKOBJ)/Makefile libs links
INFO_FILES = cpp.info cppinternals.info gcc.info gccinstall.info \
	gccint.info gfortran.info libgomp.info libitm.info \
	libquadmath.info

BUILDDEPS =
LIBDEPS = gmp mpc mpfr

######################################################################

include ../../../gar/gar.lib/info.mk
include ../../../gar/gar.lib/auto.mk
include config.mk

CONFIGURE_ARGS += \
		 --with-local-prefix=$(prefix) \
		 --with-mpfr=$(prefix) \
		 --with-gmp=$(prefix) \
		 --with-mpc=$(prefix) \
		 --with-system-zlib

CPPFLAGS += -I$(prefix)/include

GCCLIBS = libgcc libatomic libcilkrts libgfortran libgomp libitm	\
	libquadmath libsanitizer libstdc++-v3 libvtv libobjc

install-libs:
	for lib in $(GCCLIBS); do \
		$(INSTALL_ENV) $(MAKE) -C $(WORKOBJ) $(MAKE_ARGS) $(INSTALL_ARGS) install-target-$$lib; \
	done
	$(MAKECOOKIE)

install-links:
	$(SYMLINK_BIN) $(prefix)/bin/gcc cc 
	[ -x $(packageprefix)/bin/gfortran ] && \
		$(SYMLINK_BIN) $(prefix)/bin/gfortran f95
	$(MAKECOOKIE)
