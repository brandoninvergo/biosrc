<!-- README -->

<!-- Copyright (C) 2014 Brandon Invergo <brandon@invergo.net> -->

<!-- Author: Brandon Invergo <brandon@invergo.net> -->

<!-- This program is free software; you can redistribute it and/or -->
<!-- modify it under the terms of the GNU General Public License -->
<!-- as published by the Free Software Foundation; either version 3 -->
<!-- of the License, or (at your option) any later version. -->

<!-- This program is distributed in the hope that it will be useful, -->
<!-- but WITHOUT ANY WARRANTY; without even the implied warranty of -->
<!-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the -->
<!-- GNU General Public License for more details. -->

<!-- You should have received a copy of the GNU General Public License -->
<!-- along with this program. If not, see <http://www.gnu.org/licenses/>. -->


# BioSRC

## Introduction

BioSRC provides a simple way to install the latest versions of common
bioinformatics packages in their own directory (on an existing
system).  This is particularly useful when you want to install them on
a system for which you do not have administrative rights, such as a
shared cluster.  It simplifies the process of maintaining a collection
of locally installed software by cleanly separating the files of each
package (and each package version), and installing symbolic links to
the files in the directory of your choice.

### Getting started

First, clone the [Git repository](https://gitorious.org/biosrc/biosrc/):

    $ git clone git://gitorious.org/biosrc/biosrc.git

You can stay up-to-date with the latest packages and releases by
pulling in changes with Git:

    $ git pull origin master

Now that you have BioSRC, you must first configure it. From a shell,
type:

    $ cd biosrc
    $ ./bootstrap
    $ ./configure --prefix=$HOME/bio

In this example, all software will be installed to the `bio`
sub-directory of your home directory.  You may choose to install the
software to any location you like by setting the `--prefix` argument.

You will find the available packages under the `pkg`
sub-directory.  They are organized into sub-categories:

* `bio`: bioinformatics tools
* `libs`: science-related development libraries
* `tools`: general tools that may be useful in bioinformatics
           pipelines that may not be installed on most systems already
* `dev`: core compilers/interpreters for common programming languages
  and environments

To install a package, one can simply use `make`:

    $ make -C pkg/bio/hmmer install

Alternatively, you can simply use the provided `biosrc` script:

    $ biosrc install hmmer

The script also provides other useful functionality, like searching
for packages, updating them or uninstalling them.

For more information, type `make help` or view the documentation with
`info biosrc`.

## Featured software

If a program is missing from BioSRC, please send a message to the
mailing list (see "Getting help", below) to request its addition.
Similarly, if you notice that a program version is out-of-date, please
send a message.

Only software that is licensed under an accepted free software license
will be included in BioSRC.  Lamentably, many bioinformatics programs
are released in binary format only, under proprietary licenses, under
otherwise restrictive licenses ("...for academic use only...") or,
just as bad, with no explicit license at all (which effectively makes
the software proprietary).  Distributing binary-only files is
tantamount to hiding your scientific methods, while restrictive,
proprietary licenses undermine the principles of open and
collaborative science.  Thus, the use of such programs should not be
encouraged.  Finally, anyone installing software with BioSRC should be
confident that they have full permission to use the software in any
way they like.  We don't want to provide software that not everyone
can use!  If you wish to have a program added to BioSRC that is
proprietary or under a restrictive license, please contact the
software's author(s) to encourage them to re-release the software
under an appropriate license.

## Supported systems

BioSRC is geared towards installing software on GNU/Linux systems.  It
may be possible to use it on other Unix-oid operating systems, however
note that you might need to modify some files to make them work.
Patches to support building on other systems will only be accepted if
they do not disrupt building on GNU/Linux systems.

BioSRC almost certainly will not work on Windows.  You might have some
luck with CygWin, but no support can be given in that regard.

## Getting help

To get help, report bugs, request new packages or updates, etc. please
send a message to the
[mailing list](http://lists.invergo.net/listinfo.cgi/biosrc-invergo.net).

## Copying

BioSRC is based on the GNU Source Release Collection (GSRC), which is
in turn based on the GAR build system by Nick Moffitt and GARstow
enhancements by Adam Sampson.

Files from GAR and GARstow are under the following license unless
otherwise noted:

>  Copyright (C) 2001 Nick Moffitt  
>  Copyright (C) 2004, 2005, 2006, 2007, 2008, 2009, 2010 Adam Sampson
>
>  Redistribution and/or use, with or without modification, is
>  permitted.  This software is without warranty of any kind.  The
>  author(s) shall not be liable in the event that use of the
>  software causes damage.

Files specific to BioSRC, including the build recipes, are Copyright
(C) Brandon Invergo and licensed under the GNU General Public License,
Version 3, or (at your option) any later version. Please see the file
`COPYING` for more information.
 
